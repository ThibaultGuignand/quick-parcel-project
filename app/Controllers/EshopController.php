<?php

namespace App\Controllers;

class EshopController {

    public function index(): string
    {
        return require 'resources/product-list.php';
    }
    public function product(): string
    {
        return require 'resources/product.php';
    }
    public function register(): string
    {
        return require 'resources/register.php';
    }
    public function login(): string
    {
        return require 'resources/login.php';
    }
    public function cart(): string
    {
        return require 'resources/cart.php';
    }
}