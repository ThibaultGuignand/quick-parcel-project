<?php

namespace App\Middleware;

class AuthenticationMiddleware
{
    static public function checkIfUserIsLoggedIn()
    {
        if (isset($_SESSION['loggedin']) && $_SESSION['loggedin']) {
            return true;
        } else {
            header('Location:/login');
            exit;
        }
    }
}