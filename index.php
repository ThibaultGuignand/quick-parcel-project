<?php

use App\Controllers\EshopController;
use App\Controllers\ProductController;

require_once './vendor/autoload.php';

session_start();

require_once './vendor/autoload.php';

if ( !isset($_SESSION["cart"]) ) {
    $_SESSION["cart"] = 0;
}

$eshop = new EshopController();
$store = new ProductController();

switch (getUri()) {
    case '/':
        echo $eshop->index();
        break;
    case '/home':
        if (!isset($_GET['id'])) {
            header('Location:/');
            exit;
        }
        echo $eshop->index($_GET['id']);
        break;
    case '/register':
        echo $eshop->register();
        break;

    case '/product':
        echo $eshop->product();
        break;

    case '/register-user':
        $auth = new \App\Controllers\authenticationController();
        $auth->registerUser();
        break;
    case '/login':
        echo $eshop->login();
        break;

    case '/login-user':
        $auth = new \App\Controllers\authenticationController();
        $auth->loginUser();
        break;
    case '/cart':
        \App\Middleware\AuthenticationMiddleware::checkIfUserIsLoggedIn();
        echo $eshop->cart();
        break;
    case '/logout-user':
        $auth = new \App\Controllers\authenticationController();
        $auth->logOut();
        break;
case '/error':
    echo $eshop->error();
    break;
    default:
        header('HTTP/1.0 404 Not Found');
        echo '404 Not Found';
        break;
    case '/article':
        $store->show($_GET['id']);
        break;
    case '/panier':
        $store->cart();
        break;
    case '/addCart':
        echo $store->addToCart();
        break;
}