<?php

namespace Databases\Migrations;

use Databases\Connection;

final class CreateUserTable extends Connection {

    public function handle()
    {

        $query = 'CREATE TABLE users (
id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
username VARCHAR(50) NOT NULL UNIQUE,
password VARCHAR(255) NOT NULL,
created_at DATETIME DEFAULT CURRENT_TIMESTAMP
)';
        $statement = $this->connection->prepare($query);
        $statement->execute();
    }
}