<?php

namespace Databases\Migrations;

use Databases\Connection;


final class AlterCartsTable extends Connection
{
    public function handle()
    {
        $query = "ALTER TABLE carts ADD CONSTRAINT carts_user_id_foreign FOREIGN KEY (user_ID) REFERENCES users (ID)";
        $pdo = $this->connection;
        $stmt = $pdo->prepare($query);
        $stmt->execute();
    }
}


