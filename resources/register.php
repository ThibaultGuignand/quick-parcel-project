<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta content="ie=edge" http-equiv="x-ua-compatible">
    <link href="resources/src/styles.css" rel="stylesheet" type="text/css"/>
    <!-- Canonical -->
    <link href="https://www.example.com" rel="canonical">
    <!-- Robots -->
    <meta content="noindex, nofollow" name="robots">
    <!-- Device -->
    <!-- <meta name="viwport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no"> -->
    <meta content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=5" name="viewport">
    <meta content="telephone=no" name="format-detection">
    <!-- Title -->
    <title>Login</title>
    <!-- Description -->
    <meta content="Home description." name="description">
    <!-- Social -->
    <!-- Twitter -->
    <meta content="summary_large_image" name="twitter:card">
    <meta content="Quick Parcel Project — Home" name="twitter:title">
    <meta content="Home description." name="twitter:description">
    <meta content="#" name="twitter:image">
    <!-- Facebook -->
    <meta content="website" property="og:type">
    <meta content="https://www.example.com" property="og:url">
    <meta content="Quick Parcel Project — Home" property="og:title">
    <meta content="Home description." property="og:description">
    <meta content="#" property="og:image">
    <meta content="1200" property="og:image:width">
    <meta content="630" property="og:image:height">
    <!-- Favicon -->
    <meta content="#fff" name="theme-color">
    <link href="https://fonts.googleapis.com" rel="preconnect">
    <link crossorigin href="https://fonts.gstatic.com" rel="preconnect">
    <link href="https://fonts.googleapis.com/css2?family=Fanwood+Text:ital@0;1&family=Tenor+Sans&display=swap"
          rel="stylesheet">
    <link href="https://api.fontshare.com/v2/css?f[]=styro@500,600,300,400&display=swap" rel="stylesheet">
    <link href="https://api.fontshare.com/v2/css?f[]=epilogue@400,300,200,201,301,500&display=swap" rel="stylesheet">

</head>
<body>
<div class="parent">
    <div class="nav">
        <div class="title"><a href="/">
            <h1> Foxie</h1></a>
        </div>
    </div>x'
    <div class="left">
        <div class="carre"><img alt="foxes" src="resources/assets/fox-2.jpeg"></div>
    </div>
    <div class="right">
        <div class="container">
            <h2>Register</h2>
            <form action="/register-user" method="post">
                <label for="emailAddress">Email</label><br>
                <input id="emailAddress" name="username" placeholder="Your email" type="email" required> <br>
                <label for="password">Password</label> <br>
                <input id="password" name="password" placeholder="Your password" type="password" required><br>
                <button type="submit">Register</button>
            </form>
        </div>
    </div>
</div>
</body>
</html>