<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <link href="resources/src/styles.css" rel="stylesheet" type="text/css"/>
    <meta content="ie=edge" http-equiv="x-ua-compatible">
    <!-- Canonical -->
    <link href="https://www.example.com" rel="canonical">
    <!-- Robots -->
    <meta content="noindex, nofollow" name="robots">
    <!-- Device -->
    <!-- <meta name="viwport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no"> -->
    <meta content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=5" name="viewport">
    <meta content="telephone=no" name="format-detection">
    <!-- Title -->
    <title>Home</title>
    <!-- Description -->
    <meta content="Home description." name="description">
    <!-- Social -->
    <!-- Twitter -->
    <meta content="summary_large_image" name="twitter:card">
    <meta content="Quick Parcel Project — Home" name="twitter:title">
    <meta content="Home description." name="twitter:description">
    <meta content="#" name="twitter:image">
    <!-- Facebook -->
    <meta content="website" property="og:type">
    <meta content="https://www.example.com" property="og:url">
    <meta content="Quick Parcel Project — Home" property="og:title">
    <meta content="Home description." property="og:description">
    <meta content="#" property="og:image">
    <meta content="1200" property="og:image:width">
    <meta content="630" property="og:image:height">
    <!-- Favicon -->
    <meta content="#fff" name="theme-color">
    <link href="https://fonts.googleapis.com" rel="preconnect">
    <link crossorigin href="https://fonts.gstatic.com" rel="preconnect">
    <link href="https://fonts.googleapis.com/css2?family=Fanwood+Text:ital@0;1&family=Tenor+Sans&display=swap"
          rel="stylesheet">
    <link href="https://api.fontshare.com/v2/css?f[]=styro@500,600,300,400&display=swap" rel="stylesheet">
    <link href="https://api.fontshare.com/v2/css?f[]=epilogue@400,300,200,201,301,500&display=swap" rel="stylesheet">
</head>
<body class="product-list">
<nav>
    <ul>
        <li><h1>Foxie</h1></li>
        <li>BUY & SAVE A FOX</li>
        <li><?= $_SESSION['toggle'] ?? 'OFF' ?></li>
        <li><a href="/cart">CART</a></li>
    </ul>
</nav>

<div class="wrapper">
    <div class="wrapper">
        <?php if (isset($items)) {
            foreach ($items as $product) { ?>
                <div class="container-product">
                    <img src="<?= $product['image'] ?>" alt="<?= $product['name'] ?>">
                    <a href="/product?id=<?= $product['ID'] ?>">
                        <div class="description"><?= $product['name']?></div>
                    </a>
                </div>
            <?php } } ?>
    </div>


<!--    <div class="container-product">-->
<!--        <img src="./resources/assets/childfox.png" alt="Doudou">-->
<!--        <a href="/product">-->
<!--            <div class="description">Doudou Fox</div>-->
<!--        </a>-->
<!--    </div>-->
<!--    <div class="container-product">-->
<!--        <img src="./resources/assets/craftfox.png" alt="Fox Papercraft Suncatcher">-->
<!--        <a href="/product">-->
<!--            <div class="description">Fox Papercraft Suncatcher-->
<!--            </div>-->
<!--        </a>-->
<!--    </div>-->
<!--    <div class="container-product">-->
<!--        <img src="./resources/assets/pinsfox.png" alt="Golden enamelled pin Fox">-->
<!--        <a href="/product">-->
<!--            <div class="description">Enamelled pin Fox-->
<!--            </div>-->
<!--        </a>-->
<!--    </div>-->
<!--    <div class="container-product">-->
<!--        <img src="./resources/assets/woodfox.png" alt="Wooden Fox Decoration">-->
<!--        <a href="/product">-->
<!--            <div class="description">Wooden Fox Decoration</div>-->
<!--        </a>-->
<!--    </div>-->
<!--    <div class="container-product">-->
<!--        <img src="./resources/assets/handmadefox.png" alt="Fox - Magnetic needle minder">-->
<!--        <a href="/product">-->
<!--            <div class="description">Fox magnetic needle minder</div>-->
<!--        </a>-->
<!--    </div>-->
<!--    <div class="container-product">-->
<!--        <img src="./resources/assets/earsfox.png" alt="Fox earrings ">-->
<!--        <a href="/product">-->
<!--            <div class="description">Fox earrings</div>-->
<!--        </a>-->
<!--    </div>-->
<!--    <div class="container-product">-->
<!--        <img src="./resources/assets/forestfox.png" alt="FOREST FOX Necklace">-->
<!--        <a href="/product">-->
<!--            <div class="description">Forest fox necklace</div>-->
<!--        </a>-->
<!--    </div>-->
</div>
</body>
</html>